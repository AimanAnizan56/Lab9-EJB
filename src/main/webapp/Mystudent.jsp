<%--
  Created by IntelliJ IDEA.
  User: Aiman
  Date: 4/11/2021
  Time: 4:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Mystudent Page</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <%@include file="Menu.html"%>
    <div class="container">
        <h1><%= "This is mystudent page" %> </h1>
        <jsp:include page="MyStudentServlet"/>
    </div>
</body>
</html>
