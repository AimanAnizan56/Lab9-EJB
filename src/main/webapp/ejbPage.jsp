<%--
  Created by IntelliJ IDEA.
  User: Aiman
  Date: 22/1/2022
  Time: 9:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EJB Page</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<%@include file="Menu.html"%>
<sql:setDataSource var="conn" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost/myinfo" user="root" password=""/>

<sql:query var="result" dataSource="${conn}">
    SELECT * FROM myself;
</sql:query>

<div class="container">
    <h1>Data from DB is using myself class as EJB</h1>
    <table>
        <tr>
            <th>No</th>
            <th>myName</th>
            <th>myAge</th>
            <th>myHobbies</th>
            <th>myMatricNo</th>
            <th>myProgramCode</th>
            <th>myCampus</th>
            <th>myDad</th>
            <th>myMom</th>
            <th>mySiblings</th>
        </tr>

        <c:choose>
            <c:when test="${result.rowCount == 0}">
                <tr><td colspan="10" style="text-align: center">Nothing in database</td></tr>
            </c:when>
            <c:otherwise>
                <c:set var="count" value="1"/>
                <c:forEach var="row" items="${result.rows}">
                    <jsp:useBean id="myself" class="com.csclab.lab9ejb.MySelf">
                        <jsp:setProperty name="myself" property="myName" value="${row.myName}"/>
                        <jsp:setProperty name="myself" property="myAge" value="${row.myAge}"/>
                        <jsp:setProperty name="myself" property="myHobbies" value="${row.myHobbies}"/>
                    </jsp:useBean>

                    <jsp:useBean id="mystudent" class="com.csclab.lab9ejb.MyStudent">
                        <jsp:setProperty name="mystudent" property="myMatricNo" value="${row.myMatricNo}"/>
                        <jsp:setProperty name="mystudent" property="myProgramCode" value="${row.myProgramCode}"/>
                        <jsp:setProperty name="mystudent" property="myCampus" value="${row.myCampus}"/>
                    </jsp:useBean>

                    <jsp:useBean id="myfamily" class="com.csclab.lab9ejb.MyFamily">
                        <jsp:setProperty name="myfamily" property="myDad" value="${row.myDad}"/>
                        <jsp:setProperty name="myfamily" property="myMom" value="${row.myMom}"/>
                        <jsp:setProperty name="myfamily" property="mySiblings" value="${row.mySiblings}"/>
                    </jsp:useBean>
                    <tr>
                        <td> <c:out value="${count}"/> </td>
                        <td> <c:out value="${myself.getMyName()}"/> </td>
                        <td> <c:out value="${myself.getMyAge()}"/> </td>
                        <td> <c:out value="${myself.getMyHobbies()}"/> </td>
                        <td> <c:out value="${mystudent.getMyMatricNo()}"/> </td>
                        <td> <c:out value="${mystudent.getMyProgramCode()}"/> </td>
                        <td> <c:out value="${mystudent.getMyCampus()}"/> </td>
                        <td> <c:out value="${myfamily.getMyDad()}"/> </td>
                        <td> <c:out value="${myfamily.getMyMom()}"/> </td>
                        <td> <c:out value="${myfamily.getMySiblings()}"/> </td>
                    </tr>
                    <c:set var="count" value="${count + 1}"/>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </table>
    <a href="jdbcPage.jsp"><button>Back</button></a>
</div>
</body>
</html>
